package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Connection;

public final class formulaire_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Simple formulaire </title>\n");
      out.write("        \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <p><a>Formulaire</a>|<a href=\"tableau.jsp\">Liste</a></p>\n");
      out.write("        <form method=\"POST\" action=\"formulaire.jsp\"> \n");
      out.write("            <input type=\"text\" name=\"num\" placeholder=\"Numero\"/>\n");
      out.write("            <input type=\"text\" name=\"nom\" placeholder=\"Nom\"/>\n");
      out.write("            <!--<input type=\"text\" name=\"cin\" placeholder=\"CIN\"/>\n");
      out.write("            <input type=\"text\" name=\"dateNais\" placeholder=\"Date de naissance\"/>\n");
      out.write("            <input type=\"text\" name=\"situation\" placeholder=\"Situation\"/>\n");
      out.write("            <input type=\"text\" name=\"sport\" placeholder=\"Sport\"/>\n");
      out.write("            <input type=\"text\" name=\"loisir\" placeholder=\"loisir\"/>\n");
      out.write("            <input type=\"text\" name=\"adresse\" placeholder=\"adresse\"/>-->\n");
      out.write("            <input type=\"submit\" name=\"alefa\" value=\"envoyer\">\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("   ");

       if (request.getParameter("alefa")!=null) {
           int num= Integer.parseInt(request.getParameter("num"));
           String nom= request.getParameter("nom");
           Class.forName("org.gjt.mm.mysql.Driver");
           Connection con= DriverManager.getConnection("jdbc:mysql://localhost/jspform", "root","");
           String sql="insert into etudiant(id , nom) values('"+num+"' ,'"+nom+"' ) ";
           Statement st=con.createStatement();
           st.executeUpdate(sql);
            
       
       }

   
   
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
